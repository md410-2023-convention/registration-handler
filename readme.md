# Lions MD410 2023 Oceans of Opportunity Convention Registration Handler

Tooling to process [reg-form-server](https://gitlab.com/md410-2023-convention/reg-form-server) generated Lions MD410 2023 Oceans of Opportunity Convention registration form dictionaries from an [AWS SQS queue](https://aws.amazon.com/sqs/) queue to produce a PDF registration record and email it to registrees.

# Operation

Several environment variables are required:

* **EMAIL_SENDER**: The email address to use in the from: field
* **EMAIL_BCCS**: A ;-separated list of email addresses to BCC onto the emails, intended to be organisers
* **SENDGRID_API_KEY**: An API key for Sendgrid, which utlimately sends the emails
* **MONGODB_HOST**: Host of the mongodb server holding registration data
* **MONGODB_PORT**: Port of the mongodb server holding registration data
* **MONGODB_USERNAME**: Username for the mongodb server holding registration data
* **MONGODB_PASSWORD**: Passord for the mongodb server holding registration data
* **AWS_ACCESS_KEY_ID**: Access key for an AWS IAM user permitted to read the SQS queue
* **AWS_SECRET_ACCESS_KEY**: Secret key for an AWS IAM user permitted to read the SQS queue
