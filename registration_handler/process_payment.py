import rich_click as click
from rich import print

import create_payment_record
import emailer
import mongo
import reg_entry_file_utils
from rich.prompt import Confirm


@click.command()
@click.argument("reg_num", type=int)
@click.argument("amount", type=float)
def main(reg_num, amount):
    mongo.__insert_payment(reg_num, amount)
    mongo.__download_reg_entry(reg_num)
    mongo.__download_payment_entries(reg_num)
    reg = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    payments = reg_entry_file_utils.get_model_from_payments_entry(reg_num)
    pth = create_payment_record.build_pdf_from_models(reg, payments)
    if Confirm.ask("Send email?"):
        emailer.send_payments_email(reg, pth)


if __name__ == "__main__":
    main()
