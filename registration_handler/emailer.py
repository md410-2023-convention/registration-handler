from collections import namedtuple
from pathlib import Path

import rich_click as click
from md410_2023_conv_common import emailer
from rich import print

import reg_entry_file_utils


def send_reg_email(registration_model, pdf_path):
    with open(
        f"registration_email.txt",
        "r",
    ) as fh:
        template = fh.read()

    print(registration_model.emails)
    if any(registration_model.emails):
        emailer.send_mail(
            registration_model.emails,
            f"Confirmation of registration {registration_model.reg_num_string} for the Lions 2023 MD410 Convention for {registration_model.names}",
            template.format(registration=registration_model),
            [pdf_path],
        )
    else:
        print(emailer.BCCS)
        # no email addresses available
        emailer.send_mail(
            emailer.BCCS,
            f"2023 MDC - no email addresses for {registration_model.reg_num_string}",
            "Submitted details attached.",
            [pdf_path],
            bccs=None,
        )


def send_payments_email(registration_model, pdf_path):
    with open(
        f"payments_email.txt",
        "r",
    ) as fh:
        template = fh.read()

    print(registration_model.emails)
    if any(registration_model.emails):
        emailer.send_mail(
            registration_model.emails,
            f"Payments received for the Lions 2023 MD410 Convention for {registration_model.names}",
            template.format(registration=registration_model),
            [pdf_path],
        )
    else:
        # no email addresses available
        emailer.send_mail(
            emailer.BCCS,
            f"2023 MDC - no email addresses for {registration_model.reg_num_string}",
            "Submitted details attached.",
            [pdf_path],
        )


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    pass


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_reg_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    send_reg_email(registration, Path(pdf_path))


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_partial_reg_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_partial_reg_entry(reg_num)
    send_reg_email(registration, Path(pdf_path))


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_payment_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    send_payments_email(registration, Path(pdf_path))


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_partial_payment_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_partial_reg_entry(reg_num)
    send_payments_email(registration, Path(pdf_path))


if __name__ == "__main__":
    cli()
