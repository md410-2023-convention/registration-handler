""" Build MDC2022 registration record markdown and PDF files from a supplied Registration model
"""


import attr
import os.path
from pathlib import Path

import reg_entry_file_utils

import click
from md410_2023_conv_common import models, constants, build_pdf
from rich import print


DESCRIPTIONS = {
    "reg": "Registration",
    "pins": "Convention Pin",
}


@attr.s
class RegistrationRenderer(object):
    registration = attr.ib(models.Registration)
    out_dir = attr.ib(default=None)

    def __payment_details(self):
        self.out.append(f"\\newpage\n# Payment Details {{-}}")
        self.out.append(
            f"""\

Please make all payments to this account:

* **Bank**: Nedbank
* **Branch Code**: 198765
* **Account Number**: 2015836799
* **Account Type**: Savings account
* **Account Name**: Convention 2020

Please make EFT payments rather than cash deposits wherever possible.

The name of the account is deliberately "Convention **2020**" as the account was opened for the 2020 MD Convention intended to be held in-person in Durban and then kept available.

Use the reference "*{self.registration.reg_num_string}*" when making payment. 

Payments can be made in full or in any number of instalments. Please make full payment as soon as possible.

Please send proof of payment to [vanwykk+mdc2023@gmail.com](mailto:vanwykk+mdc2023@gmail.com).

## Cancellations {{-}}

* If your registration is cancelled earlier than 30 days before your date of arrival, your payment will be refunded in full except for a R50 admin fee.
* Cancellations later than 30 days before your date of arrival will not be refunded as the full expenses will already have been incurred for the registration.

Thank you again for registering for the 2023 MD410 Oceans of Opportunity Convention.
"""
        )

    def __attrs_post_init__(self):
        self.out = [
            f"# Application Number{'s' if len(self.registration.attendees) > 1 else ''}: {self.registration.reg_num_string} {{-}}",
        ]

        self.out.append(
            f"## Attendee Details - Registered on {self.registration.timestamp:%d/%m/%y at %H:%M} {{-}}"
        )
        for n, attendee in enumerate(self.registration.attendees, 1):
            self.attendee = attendee
            self.out.append(f"")
            self.out.append(f"### Attendee {n} {{-}}")
            self.render_attendee()
        self.name = f"{self.registration.names.replace(' ','_').lower()}"
        self.out.append("")
        self.out.append("## Registration Details {-}")
        self.out.append("")
        self.render_items()
        self.out.append("")
        self.out.append(f"# Total Cost: R{self.registration.cost} {{-}}")
        self.out.append("")
        self.out.append("")
        self.__payment_details()
        name = self.name.replace("'", "")
        self.fn = f"mdc2023_registration_{self.registration.reg_num:03}_{name}.txt"
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)
        self.save()

    def append(self, msg):
        self.out.append(msg.encode("utf-8", "ignore").decode("utf-8"))

    def render_attendee(self):
        self.append(
            f"* **Registration Number:** {self.registration.reg_num:003}/{self.attendee.attendee_num}"
        )
        self.append(f"* **First Name(s):** {self.attendee.first_names}")
        self.append(f"* **Last Name:** {self.attendee.last_name}")
        if self.attendee.cell:
            self.append(f"* **Cell Phone:** {self.attendee.cell}")
        if self.attendee.email:
            self.append(f"* **Email Address:** {self.attendee.email}")
        if self.attendee.lion:
            self.append(f"* **Club:** {self.attendee.club}")
        self.append(
            f"* **Dietary Requirements:** {self.attendee.dietary if self.attendee.dietary else 'None'}"
        )
        self.append(
            f"* **Disabilities:** {self.attendee.disability if self.attendee.disability else 'None'}"
        )
        if not self.attendee.lion:
            self.append(
                f"* **Interested in a Partner's Program:** {'Yes' if self.attendee.partner_program else 'No'}"
            )
        self.append(
            f"* **Attendee will attend the Melvin Jones lunch:** {'Yes' if self.attendee.mjf_lunch else 'No'}"
        )
        self.append(
            f"* **Attendee will attend the PDG's Breakfas:** {'Yes' if self.attendee.pdg_dinner else 'No'}"
        )
        self.append(
            f"* **Attendee will attend the beach cleanup service project:** {'Yes' if self.attendee.beach_cleanup else 'No'}"
        )
        if self.attendee.lion:
            self.append(
                f"* **Attendee will attend the President Elect's Breakfast:** {'Yes' if self.attendee.lpe_breakfast else 'No'}"
            )
        self.append(f"* **Details On Name Badge:** {self.attendee.name_badge}")
        if self.attendee.auto_name_badge:
            self.append("")
            self.append(
                f"**The name badge details were generated from the first and last names because no name badge details were supplied on the registration form. Please contact the registration team if you would like to update these details.**"
            )
            self.append("")
            self.append("")

    def render_items(self):
        for item, number in self.registration.items.dict().items():
            if number:
                cost = getattr(constants, f"COST_{item.upper()}", 0) * number
                self.append(
                    f"* **{number} {DESCRIPTIONS[item]}{'s' if number > 1 else ''}:** R{cost}"
                )

    def save(self):
        with open(self.fn, "w") as fh:
            fh.write("\n".join(self.out))


def build_pdf_from_model(registration_model, debug=False, out_dir="."):
    renderer = RegistrationRenderer(registration_model, out_dir)
    if debug:
        print(registration_model)
        print(renderer.fn)
    if out_dir == ".":
        out_dir = os.getcwd()
    pdf_fn = build_pdf.build_pdf(out_dir, renderer.fn, pull=False, debug=False)
    if debug:
        print(pdf_fn)
    return Path(pdf_fn)


@click.command()
@click.argument("reg_num", type=int)
def build_pdf_from_reg_entry(reg_num):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    pth = build_pdf_from_model(registration, out_dir=".")
    print(f"Registration record for reg num {reg_num:03} written to {pth}")


if __name__ == "__main__":
    build_pdf_from_reg_entry()
