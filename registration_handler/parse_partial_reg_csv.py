import csv
from datetime import datetime
import json

import mongo

from md410_2023_conv_common import models
from rich import print


with open("230415_1152_partial_registrations.csv", "r") as fh:
    c = csv.DictReader(fh)
    rn = mongo.get_next_partial_reg_num()
    for r in c:
        d = {}
        for a in (
            "first_names",
            "last_name",
            "name_badge",
            "cell",
            "email",
            "dietary",
            "disability",
            "club",
        ):
            d[a] = r[f"main_{a}"]
        for a in ("mjf_lunch", "pdg_dinner", "beach_cleanup", "lpe_breakfast"):
            d[a] = bool(r[f"main_{a}"])
        d["lion"] = r["membership"] == "lion"
        pm = models.PartialAttendeeModel(**d)

        for k, v in (
            ("welcome", "welcome"),
            ("dist_conv", "dist_convention"),
            ("banquet", "banquet"),
            ("md_conv", "md_convention"),
            ("theme", "theme"),
            ("pins", "pins"),
        ):
            d[k] = int(r[v])
        pi = models.PartialRegistrationItems(**d)
        m = models.PartialRegistration(
            reg_num=rn,
            attendee=pm,
            items=pi,
            timestamp=datetime.fromisoformat(r["created_at"].rsplit(".")[0]),
        )
        d = json.loads(m.json())
        print(d)
        mongo.insert_partial_reg_form(d)
        rn += 1
