from datetime import datetime
import os

from bson import json_util
import rich_click as click
from dotenv import load_dotenv
from pymongo import MongoClient
import pyperclip
from rich import print

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["md410_2023_conv"]
reg_collection = db["reg_form"]
partial_reg_collection = db["partial_reg_form"]
payment_collection = db["payment"]
partial_payment_collection = db["partial_payment"]


def get_next_reg_num(collection=reg_collection):
    try:
        return int([d for d in collection.find().sort("reg_num")][-1]["reg_num"]) + 1
    except Exception:
        return 1


def get_next_partial_reg_num():
    return get_next_reg_num(collection=partial_reg_collection)


def insert_reg_form(reg_form_dict, collection=reg_collection):
    collection.insert_one(reg_form_dict)


def insert_partial_reg_form(reg_form_dict):
    insert_reg_form(reg_form_dict, collection=partial_reg_collection)


def get_undeleted_reg_nums():
    return [m["reg_num"] for m in reg_collection.find() if not m.get("deleted", False)]


def get_deleted_reg_nums():
    return [m["reg_num"] for m in reg_collection.find() if m.get("deleted", False)]


def __set_deletion_flag(reg_num, flag):
    d = reg_collection.find_one({"reg_num": reg_num})
    d["deleted"] = flag
    reg_collection.replace_one({"reg_num": reg_num}, d)


def __download_reg_entry(reg_num, collection=reg_collection, prefix=""):
    q = collection.find_one({"reg_num": reg_num})
    if q:
        with open(f"{prefix}reg_entry_{reg_num:03}.json", "w") as fh:
            fh.write(json_util.dumps(q))


def __download_partial_reg_entry(
    reg_num, collection=partial_reg_collection, prefix="partial_"
):
    __download_reg_entry(reg_num, collection=partial_reg_collection, prefix="partial_")


def get_all_registrations():
    for reg in reg_collection.find():
        yield reg


def get_all_partial_registrations():
    for reg in partial_reg_collection.find():
        yield reg


def get_undeleted_registrations():
    for reg in reg_collection.find({"deleted": False}):
        yield reg


def delete_payment(payment_id):
    payment_collection.delete_one({"_id": payment_id})


def delete_partial_payment(payment_id):
    partial_payment_collection.delete_one({"_id": payment_id})


def __insert_payment(reg_num, amount):
    d = {
        "reg_num": reg_num,
        "amount": amount,
        "timestamp": datetime.now().isoformat(),
    }
    payment_collection.insert_one(d)


def __insert_partial_payment(reg_num, amount):
    d = {
        "reg_num": reg_num,
        "amount": amount,
        "timestamp": datetime.now().isoformat(),
    }
    partial_payment_collection.insert_one(d)


def get_all_payments():
    for payment in payment_collection.find():
        yield payment


def get_all_partial_payments():
    for payment in partial_payment_collection.find():
        yield payment


def __download_payment_entries(reg_num):
    q = payment_collection.find({"reg_num": reg_num})
    if q:
        with open(f"payments_{reg_num:03}.json", "w") as fh:
            fh.write(json_util.dumps(q))


def __download_partial_payment_entries(reg_num):
    q = partial_payment_collection.find({"reg_num": reg_num})
    if q:
        with open(f"partial_payments_{reg_num:03}.json", "w") as fh:
            fh.write(json_util.dumps(q))


def upload_payment_entries(reg_num):
    with open(f"payments_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    payment_collection.replace_one({"reg_num": reg_num}, d)


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    pass


@cli.command()
def all_emails():
    emails = set(
        [
            e
            for e in [
                m["attendees"][0].get("email", None) for m in reg_collection.find()
            ]
            if e
        ]
    )
    pyperclip.copy(";".join(emails))
    print(f"{len(emails)} emails copied to the clipboard")


@cli.command()
@click.argument("reg_num", type=int)
def download_reg_entry(reg_num):
    __download_reg_entry(reg_num)


@cli.command()
@click.argument("reg_num", type=int)
def upload_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    reg_collection.replace_one({"reg_num": reg_num}, d)


@cli.command()
@click.argument("reg_num", type=int)
def upload_partial_reg_entry(reg_num):
    with open(f"partial_reg_entry_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    partial_reg_collection.replace_one({"reg_num": reg_num}, d)


@cli.command()
@click.argument("reg_num", type=int)
def insert_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    reg_collection.insert(d)


@cli.command()
@click.argument("reg_num", type=int)
def download_partial_reg_entry(reg_num):
    __download_partial_reg_entry(reg_num)


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("amount", type=float)
def insert_payment(reg_num, amount):
    __insert_payment(reg_num, amount)


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("amount", type=float)
def insert_partial_payment(reg_num, amount):
    __insert_payment(reg_num, amount)


@cli.command()
@click.argument("reg_num", type=int)
def delete_reg_entry(reg_num):
    __set_deletion_flag(reg_num, True)


@cli.command()
@click.argument("reg_num", type=int)
def undelete_reg_entry(reg_num):
    __set_deletion_flag(reg_num, False)


@cli.command()
@click.argument("reg_num", type=int)
def download_payment_entries(reg_num):
    __download_payment_entries(reg_num)


@cli.command()
@click.argument("reg_num", type=int)
def download_partial_payment_entries(reg_num):
    __download_partial_payment_entries(reg_num)


if 1:
    if __name__ == "__main__":
        cli()

if 0:
    print([p for p in get_all_payments()])
    p = {
        "reg_num": 1,
        "amount": 250.00,
        "timestamp": datetime.now().isoformat(),
    }
    insert_payment(p)
    print([p for p in get_all_payments()])

if 0:
    for payment in [p for p in get_all_payments()]:
        delete_payment(payment["_id"])
    print([p for p in get_all_payments()])

if 0:
    print(get_next_partial_reg_num())
    print([p for p in get_all_partial_registrations()])

if 0:
    delete_partial_payment(list(get_all_partial_payments())[-1]["_id"])

if 0:
    print(list(get_all_partial_payments()))
