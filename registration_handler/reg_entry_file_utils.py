""" Helper functions to work with local reg_entry files, downloaded from mongo
"""

import json

from md410_2023_conv_common import models


def get_model_from_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json.loads(fh.read())
    registration = models.Registration(**d)
    return registration


def get_model_from_partial_reg_entry(reg_num):
    with open(f"partial_reg_entry_{reg_num:03}.json", "r") as fh:
        d = json.loads(fh.read())
    registration = models.PartialRegistration(**d)
    return registration


def get_model_from_payments_entry(reg_num):
    with open(f"payments_{reg_num:03}.json", "r") as fh:
        l = json.loads(fh.read())
    payments = [models.Payment(**d) for d in l]
    return payments


def get_model_from_partial_payments_entry(reg_num):
    with open(f"partial_payments_{reg_num:03}.json", "r") as fh:
        l = json.loads(fh.read())
    payments = [models.PartialPayment(**d) for d in l]
    return payments
