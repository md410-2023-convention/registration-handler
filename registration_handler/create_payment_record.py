""" Build MDC2022 payment record markdown and PDF files from supplied Payment and Registration models
"""


import attr
import json
import os, os.path
from pathlib import Path

import reg_entry_file_utils

import click
from md410_2023_conv_common import models, constants, build_pdf
from rich import print


@attr.s
class Renderer(object):
    registration = attr.ib(models.Registration)
    payments = attr.ib(factory=list)
    out_dir = attr.ib(default=None)
    partial = attr.ib(default=False)

    def __payment_details(self):
        self.out.append(f"# Payment Details {{-}}")
        self.out.append(
            f"""\

Please make all payments to this account:

* **Bank**: Nedbank
* **Branch Code**: 198765
* **Account Number**: 2015836799
* **Account Type**: Savings account
* **Account Name**: Convention 2020

Please make EFT payments rather than cash deposits wherever possible.

Use the reference "*{self.registration.reg_num_string}*" when making payment. 

The name of the account is deliberately "Convention **2020**" as the account was opened for the 2020 MD Convention intended to be held in-person in Durban and then kept available.

Payments can be made in full or in any number of instalments. Please make full payment by 20 March 2023.

Please send proof of payment to [vanwykk+mdc2023@gmail.com](mailto:vanwykk+mdc2023@gmail.com).

Thank you again for registering for the 2023 MD410 Oceans of Opportunity Convention.
"""
        )

    def __attrs_post_init__(self):
        self.name = f"{self.registration.names.replace(' ','_').lower()}"

        self.out = ["# MD410 Convention 2023 Payment Record {-}"]
        names = self.registration.names.replace("'", '"')
        self.out.append(
            f"## {self.registration.names} - {self.registration.reg_num_string} {{-}}",
        )
        if self.payments:
            self.out.append(f"")
            self.out.append(f"|Details |Amount |")
            self.out.append(f"|:-|-:|")
            self.out.append(f"|**Initial Balance** | R{self.registration.cost:0.2f}|")
            self.owed = self.registration.cost
            for payment in self.payments:
                self.out.append(
                    f"|Payment recorded on {payment.timestamp:%y/%m/%d %H:%M} | R{payment.amount:0.2f}|"
                )
                self.owed -= payment.amount
            self.out.append(f"|**Total Owed** | R{self.owed:0.2f}|")
            self.out.append(f"")
        self.__payment_details()
        name = self.name.replace("'", "").replace('"', "")
        self.fn = f"mdc2023_payments_{'p' if self.partial else ''}{self.registration.reg_num:03}_{name}.txt"
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)
        self.save()

    def append(self, msg):
        self.out.append(msg.encode("ascii", "ignore").decode("ascii"))

    def save(self):
        with open(self.fn, "w") as fh:
            fh.write("\n".join(self.out))


def build_pdf_from_models(registration_model, payment_models, debug=False, out_dir="."):
    renderer = Renderer(
        registration_model,
        payment_models,
        out_dir,
        partial=hasattr(registration_model, "attendee"),
    )
    if debug:
        print(registration_model)
        print(payment_models)
        print(renderer.fn)
    if out_dir == ".":
        out_dir = os.getcwd()
    pdf_fn = build_pdf.build_pdf(out_dir, renderer.fn, pull=False, debug=False)
    if debug:
        print(pdf_fn)
    return Path(pdf_fn)


@click.command()
@click.argument("reg_num", type=int)
@click.option("--partial", is_flag=True, default=False)
@click.option("--single-attendee", default=None)
def build_pdf_from_reg_entry(reg_num, partial=False, single_attendee=None):
    f = {
        False: (
            reg_entry_file_utils.get_model_from_reg_entry,
            reg_entry_file_utils.get_model_from_payments_entry,
        ),
        True: (
            reg_entry_file_utils.get_model_from_partial_reg_entry,
            reg_entry_file_utils.get_model_from_partial_payments_entry,
        ),
    }
    registration = f[partial][0](reg_num)
    payments = f[partial][1](reg_num)
    pth = build_pdf_from_models(registration, payments, debug=True, out_dir=".")
    print(f"Payment record for reg num {reg_num:03} written to {pth}")


if __name__ == "__main__":
    build_pdf_from_reg_entry()
