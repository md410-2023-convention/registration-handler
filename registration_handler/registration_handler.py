from collections import namedtuple
import copy
import json
import os
import sys

from md410_2023_conv_common import models, sqs
from rich import print

import create_reg_record
import emailer
import mongo


REGISTRATION_RECORD = namedtuple(
    "RegistrationRecord", ("registration_model", "pdf_path")
)


def process_reg_form_submissions(reg_form_data_list, test=False, debug=False):
    reg_num = mongo.get_next_reg_num()
    for reg_form_data in reg_form_data_list:
        try:
            attendee_num = 1
            reg_form_dict = json.loads(reg_form_data)
            print(reg_form_dict)
            reg_form_dict["reg_num"] = reg_num
            attendee_nums = []
            for attendee in reg_form_dict["attendees"]:
                attendee["attendee_num"] = attendee_num
                attendee_nums.append(attendee_num)
                attendee_num += 1
            reg_form_dict["attendee_nums"] = attendee_nums
            if not test:
                mongo.insert_reg_form(reg_form_dict)
            registration = models.Registration(**reg_form_dict)
            pdf_path = create_reg_record.build_pdf_from_model(registration, debug=debug)
            reg_num += 1
            print(pdf_path)
            emailer.send_reg_email(registration, pdf_path)
        except Exception as e:
            raise
            print(e)
            pass


def main(test_file=None, debug=False):
    if test_file:
        debug = True
        with open(test_file, "r") as fh:
            reg_form_data_list = [fh.read()]
    else:
        reg_form_data_list = sqs.read_reg_form_data(max_number_of_messages=10)
    process_reg_form_submissions(
        reg_form_data_list,
        test=test_file is not None,
        debug=debug,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        "MDC 2023 Registration Handler, processing entries from an SQS queue"
    )
    parser.add_argument("-v", action="store_true", help="Emit debug")
    parser.add_argument(
        "--test-file",
        default=None,
        help="Use provided test data rather than the SQS queue",
    )
    parser.add_argument(
        "--schedule-period",
        type=int,
        default=None,
        help="A period of time in seconds to wait before processing the queue again. If not given, the tool runs once and exits",
    )

    args = parser.parse_args()
    main(args.test_file, args.v)
    if args.schedule_period is not None:
        import schedule
        import time

        schedule.every(args.schedule_period).seconds.do(
            main, test_file=args.test_file, debug=args.v
        )
        while True:
            schedule.run_pending()
            time.sleep(1)
